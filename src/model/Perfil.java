package model;

public class Perfil {
	private String nome_perfil;
	private int id_perfil;

	public Perfil(String nome_perfil) {
		this.nome_perfil = nome_perfil;
	}
	
	public Perfil(int id_perfil, String nome_perfil) {
		this.id_perfil = id_perfil;
		this.nome_perfil = nome_perfil;
	}

	public String getNomePerfil() {
		return nome_perfil;
	}

	public void setNomePerfil(String nome_perfil) {
		this.nome_perfil = nome_perfil;
	}
	

	public int getIdPerfil() {
		return id_perfil;
	}

	public void setIdPerfil(int id_perfil) {
		this.id_perfil = id_perfil;
	}

	@Override
	public String toString() {
		return "Perfil [nome_perfil=" + nome_perfil + "]";
	}
	
		

}
