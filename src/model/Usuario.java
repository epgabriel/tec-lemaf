package model;

import java.util.Date;
import java.util.List;


public class Usuario {
	private int id;
	private String nome;
	private String cpf;
	private Date nascimento;
	private char sexo;
	private Cargo cargo;
	private List<Perfil> perfis;
	
	public Usuario(		
			String nome,
			String cpf,
			Date nascimento,
			char sexo,
			Cargo cargo,
			List<Perfil> perfis
	) {
		this.nome = nome;
		this.cpf = cpf;
		this.nascimento = nascimento;
		this.sexo = sexo;
		this.cargo = cargo;
		this.perfis = perfis;
	}
	
	public Usuario(	
			int id,
			String nome,
			String cpf,
			Date nascimento,
			char sexo,
			Cargo cargo,
			List<Perfil> perfis
	) {
		this.id = id;
		this.nome = nome;
		this.cpf = cpf;
		this.nascimento = nascimento;
		this.sexo = sexo;
		this.cargo = cargo;
		this.perfis = perfis;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Date getNascimento() {
		return nascimento;
	}

	public void setNascimento(Date nascimento) {
		this.nascimento = nascimento;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public List<Perfil> getPerfis() {
		return perfis;
	}

	public void setPerfis(List<Perfil> perfis) {
		this.perfis = perfis;
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Usuario [nome=" + nome + ", cpf=" + cpf + ", nascimento=" + nascimento + ", sexo=" + sexo + ", cargo="
				+ cargo + ", perfis=" + perfis + "]";
	}	

}
