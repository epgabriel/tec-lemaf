package model;

public class Cargo {
	private int id_cargo;
	private String nome_cargo;

	public Cargo(int id_cargo, String nome_cargo) {
		this.nome_cargo = nome_cargo;
		this.id_cargo = id_cargo;
	}
	
	public Cargo (String nome_cargo) {
		this.nome_cargo = nome_cargo;
		this.id_cargo = -1;
	}

	public String getNomeCargo() {
		return nome_cargo;
	}

	public void setNomeCargo(String nome_cargo) {
		this.nome_cargo = nome_cargo;
	}

	public int getIdCargo() {
		return id_cargo;
	}

	public void setIdCargo(int id_cargo) {
		this.id_cargo = id_cargo;
	}

	@Override
	public String toString() {
		return "Cargo [nome_cargo=" + nome_cargo + "]";
	}

}
