package conn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	public Connection getConnection() {		
		try {			
            return DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/desafio_lemaf?verifyServerCertificate=false&useSSL=true", "root", "sql012606");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
	}
}
