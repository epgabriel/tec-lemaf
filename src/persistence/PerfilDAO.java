package persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.PreparedStatement;

import conn.ConnectionFactory;
import model.Perfil;

public class PerfilDAO {
	
	private final String query_sel_all = "SELECT * FROM `perfil`";
	private final String query_insert  = "INSERT INTO `perfil` (`nome_perfil`) VALUES (?)";
	private final String query_update  = "UPDATE `perfil` SET `nome_perfil` = ? WHERE `nome_perfil` = ?";
	private final String query_delete  = "DELETE FROM `perfil` WHERE `nome_perfil` = ?";
	
	public PerfilDAO() {}
	
	public List<Perfil> selectAll () {		
		List<Perfil> lperfil = new ArrayList<Perfil>();
		try {
			Connection conn = new ConnectionFactory().getConnection();
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(this.query_sel_all);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int codigo = rs.getInt(1);
				String nome = rs.getString(2);
				Perfil perfil = new Perfil(codigo, nome);
				
				System.out.printf("%d - %s\n", codigo, nome);
				
				lperfil.add(perfil);
			}
			conn.close();
		} catch (Exception e) {
			System.err.println("Erro " + e.toString());
		}
		return lperfil;
	}
	
	public void insertPerfil (Perfil perfil) {
		try {
			Connection conn = new ConnectionFactory().getConnection();
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(this.query_insert);
			ps.setString(1, perfil.getNomePerfil());
			ps.execute();
			conn.close();
		} catch (Exception e) {
			System.out.println("Exce��o na inser��o " + e.toString());
		}
	}
	
	public void updatePerfil (String nome_original, String novo_nome) {
		try {
			Connection conn = new ConnectionFactory().getConnection();
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(this.query_update);
			ps.setString(1, novo_nome);
			ps.setString(2, nome_original);
			ps.execute();
			conn.close();
		} catch (Exception e) {
			System.out.println("Erro da atualiza��o: " + e.toString());
		}
	}
	
	public void deletePerfil (String nome_perfil) {
		try {
			Connection conn = new ConnectionFactory().getConnection();
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(this.query_delete);
			ps.setString(1, nome_perfil);
			ps.execute();
			conn.close();
		} catch (Exception e) {
			System.out.println("Erro na exclus�o: " + e.toString());
		}
	}

}
