package persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.PreparedStatement;

import conn.ConnectionFactory;
import model.Cargo;

public class CargoDAO {
	
	private final String query_sel_all = "SELECT * FROM `cargo`";
	private final String query_insert  = "INSERT INTO `cargo` (`nome_cargo`) VALUES (?)";
	private final String query_update  = "UPDATE `cargo` SET `nome_cargo` = ? WHERE `nome_cargo` = ?";
	private final String query_delete  = "DELETE FROM `cargo` WHERE `nome_cargo` = ?";
	
	public CargoDAO() {}
	
	public List<Cargo> selectAll () {		
		List<Cargo> lcargo = new ArrayList<Cargo>();
		try {
			Connection conn = new ConnectionFactory().getConnection();
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(this.query_sel_all);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int codigo = rs.getInt(1);
				String nome = rs.getString(2);
				Cargo cargo = new Cargo(codigo, nome);
				
				System.out.printf("%d - %s\n", codigo, nome);
				
				lcargo.add(cargo);
			}
			conn.close();
		} catch (Exception e) {
			System.err.println("Erro " + e.toString());
		}
		return lcargo;
	}
	
	public void insertCargo (Cargo cargo) {
		try {
			Connection conn = new ConnectionFactory().getConnection();
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(this.query_insert);
			ps.setString(1, cargo.getNomeCargo());
			ps.execute();
			conn.close();
		} catch (Exception e) {
			System.out.println("Exce��o na inser��o " + e.toString());
		}
	}
	
	public void updateCargo (String nome_original, String novo_nome) {
		try {
			Connection conn = new ConnectionFactory().getConnection();
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(this.query_update);
			ps.setString(1, novo_nome);
			ps.setString(2, nome_original);
			ps.execute();
			conn.close();
		} catch (Exception e) {
			System.out.println("Erro da atualiza��o: " + e.toString());
		}
	}
	
	public void deleteCargo (String nome_cargo) {
		try {
			Connection conn = new ConnectionFactory().getConnection();
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(this.query_delete);
			ps.setString(1, nome_cargo);
			ps.execute();
			conn.close();
		} catch (Exception e) {
			System.out.println("Erro na exclus�o: " + e.toString());
		}
	}

}
