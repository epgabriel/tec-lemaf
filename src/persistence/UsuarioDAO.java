package persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mysql.jdbc.PreparedStatement;

import conn.ConnectionFactory;
import model.Cargo;
import model.Usuario;

public class UsuarioDAO {
	
	private final String query_sel_all = "SELECT * FROM `usuario`";
	private final String query_insert  = "INSERT INTO `usuario` (`nome`, `cpf`, `sexo`, `nascimento`, `cargo`) VALUES (?,?,?,?,?)";
	private final String query_delete  = "DELETE FROM `usuario` WHERE `nome_usuario` = ?";
	
	public UsuarioDAO() {}
	
	public List<Usuario> selectAll () {		
		List<Usuario> lusuario = new ArrayList<Usuario>();
		try {
			Connection conn = new ConnectionFactory().getConnection();
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(this.query_sel_all);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int codigo   = rs.getInt(1);
				String nome  = rs.getString(2);
				String cpf   = rs.getString(3);
				char sexo    = rs.getString(4).charAt(0);
				Date nasc    = rs.getDate(5);
				int id_cargo = rs.getInt(6);
				
				Cargo c = new Cargo(id_cargo, "");
				
				Usuario usuario = new Usuario(codigo, nome, cpf, nasc, sexo, c, null);
				
				System.out.printf("%d - %s\n", codigo, nome);
				
				lusuario.add(usuario);
			}
			conn.close();
		} catch (Exception e) {
			System.err.println("Erro " + e.toString());
		}
		return lusuario;
	}
	
	public void insertUsuario (Usuario usuario) {
		try {
			Connection conn = new ConnectionFactory().getConnection();
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(this.query_insert);
			ps.setString(1, usuario.getNome());
			ps.setString(2, usuario.getCpf());
			ps.setString(3, usuario.getSexo()+"");
			ps.setDate(4, (java.sql.Date) usuario.getNascimento());
			ps.setInt(5, usuario.getCargo().getIdCargo());
			ps.setString(2, usuario.getCpf());
			ps.execute();
			conn.close();
		} catch (Exception e) {
			System.out.println("Exce��o na inser��o " + e.toString());
		}
	}
	
	
	public void deletePerfil (String cpf) {
		try {
			Connection conn = new ConnectionFactory().getConnection();
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(this.query_delete);
			ps.setString(1, cpf);
			ps.execute();
			conn.close();
		} catch (Exception e) {
			System.out.println("Erro na exclus�o: " + e.toString());
		}
	}
}
