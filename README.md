﻿Código fonte do desafio proposto.
Devido ao mestrado e ao meu emprego atual, não consegui elaborar o sistema completo no prazo estabelecido, então foquei no backend, modelando os dados e aplicando técnicas de MVC e POO.
Na pasta banco existe um modelo de dados em conjunto com alguns exemplos de consulta.
Utilizei o Eclipse para desenvolvimento WEB EE.
Para comunicação com o banco, após configurá-lo no MySQL Workbench, precisei adicionar o driver do mysql versão 5.1 no classpath do Eclipse.
Model e persistence foram desenvolvidos visando relacionamentos entre as tabelas.
O JSP em view é apenas um hello world, onde iria o front-end do sistema.

#Lógica desenvolvida
Para os dados relacionei a tabela de usuários com perfis e cargos na forma de chave estrangeira, também criei uma tabela auxiliar chamada perfis_usuario para relacionar os perfis que um usuário pode possuir.

Para a modelagem do sistema, imaginei criar formulários que se conectassem com o banco através dos servlets do java.


Foi desafiador relembrar como desenvolver para web utilizando java. Gostei bastante da experiência.

